package Paciente;

import java.util.Scanner;

import clases.CardioVascular;
import clases.Enfermedad;
import clases.Respiratoria;

public class Paciente {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.println("Paciente 1");

		// Metodo gravedad de una enfermedad

		Enfermedad Enfermedad1 = new Enfermedad("Sexo", "Tumor testicular", "Grave", "Dolores , esteril", 616);

		System.out.println("El origen es:" + Enfermedad1.getOrigen());
		System.out.println("El nombre es:" + Enfermedad1.getNombre());
		System.out.println("El gravedad es:" + Enfermedad1.getGravedad());
		System.out.println("El caracteristicas es:" + Enfermedad1.getCaracteristicas());
		System.out.println("El duracion es:" + Enfermedad1.getDuracion());

		System.out.println("");

		System.out.println("El tumor... es:");

		System.out.println(Enfermedad1.calcularGravedad("Tumor Cerebral"));

		System.out.println("");

		System.out.println("La Apendicitis es:");

		System.out.println(Enfermedad1.calcularGravedad("Apendicitis"));

		System.out.println("");

		System.out.println("El Resfriado es:");

		System.out.println(Enfermedad1.calcularGravedad("Resfriado"));

		System.out.println("");

		// Metodo calcularNvOxigeno
		
		System.out.println("Paciente 2");

		Respiratoria Respiratoria1 = new Respiratoria("Tabaco", "Tumor de pulmones", "Muy grave",
				"Vomitos , dolores , mareos , perdidas vision...", 365, 92.2, "Elevada");

		System.out.println("El origen es:" + Respiratoria1.getOrigen());
		System.out.println("El nombre es:" + Respiratoria1.getNombre());
		System.out.println("El gravedad es:" + Respiratoria1.getGravedad());
		System.out.println("El caracteristicas es:" + Respiratoria1.getCaracteristicas());
		System.out.println("El duracion es:" + Respiratoria1.getDuracion());
		System.out.println("El nvOxigeno es:" + Respiratoria1.getNvOxigeno());
		System.out.println("El duracion es:" + Respiratoria1.getDificultadRespiratoria());

		System.out.println("El nivel de oxigeno del Resfriado Comun es de:");
		System.out.println(Respiratoria1.calcularNivelOxigeno("Resfriado comun"));

		System.out.println("");

		System.out.println("El nivel de oxigeno de la Influenza es de:");
		System.out.println(Respiratoria1.calcularNivelOxigeno("Influenza"));

		System.out.println("");

		System.out.println("El nivel de oxigeno de la Faringitis es de:");
		System.out.println(Respiratoria1.calcularNivelOxigeno("Faringitis"));

		System.out.println("");

		System.out.println("El nivel de oxigeno de la Bronquitis es de:");
		System.out.println(Respiratoria1.calcularNivelOxigeno("Bronquitis"));

		System.out.println("");

		System.out.println("El nivel de oxigeno de la Neumon�a es de:");
		System.out.println(Respiratoria1.calcularNivelOxigeno("Neumon�a"));

		System.out.println("");

		// Metodo calcularNvOxigeno
		
		System.out.println("Paciente 3");

		CardioVascular CardioVascular1 = new CardioVascular("Huevos", "Taquicardia", "Grave",
				"Fatiga mayor frecuencia cardiaca", 8919, "Elevado", 120);

		System.out.println("El origen es:" + CardioVascular1.getOrigen());
		System.out.println("El nombre es:" + CardioVascular1.getNombre());
		System.out.println("El gravedad es:" + CardioVascular1.getGravedad());
		System.out.println("El caracteristicas es:" + CardioVascular1.getCaracteristicas());
		System.out.println("El duracion es:" + CardioVascular1.getDuracion());
		System.out.println("El nvOxigeno es:" + CardioVascular1.getNvFatiga());
		System.out.println("El duracion es:" + CardioVascular1.getNvfrecuenciaCardiaca());

		System.out.println("El nivel de fatiga de la Bradicardia  es de:");
		System.out.println(CardioVascular1.calcularNiveldeFatiga("Bradicardia"));

		System.out.println("");

		System.out.println("El nivel de fatiga de la Taquicardia es de:");
		System.out.println(CardioVascular1.calcularNiveldeFatiga("Taquicardia"));

		input.close();

	}

}
