package clases;

public class Enfermedad {
	
	//Atributos

	protected String origen;
	protected String nombre;
	protected String gravedad;
	protected String caracteristicas;
	protected int duracion;

	// Constructores

	public Enfermedad() {

		this.origen = "";
		this.nombre = "";
		this.gravedad = "";
		this.caracteristicas = "";
		this.duracion = 0;

	}

	public Enfermedad(String origen, String nombre, String gravedad, String caracteristicas, int duracion) {
		super();
		this.origen = origen;
		this.nombre = nombre;
		this.gravedad = gravedad;
		this.caracteristicas = caracteristicas;
		this.duracion = duracion;
	}

	// Setters Y Getters

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGravedad() {
		return gravedad;
	}

	public void setGravedad(String gravedad) {
		this.gravedad = gravedad;
	}

	public String getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(String caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	// To String

	public String toString() {
		return "Enfermedad [tipo=" + origen + ", nombre=" + nombre + ", gravedad=" + gravedad + ", caracteristicas="
				+ caracteristicas + ", duracion=" + duracion + "]";
	}
	
	//Metodo gravedad de una enfermedad

	public String calcularGravedad(String nombre) {

		String enf1 = "Tumor";
		String gravedad1 = "Muy Grave";

		String enf2 = "Apendicitis";
		String gravedad2 = "Aguda";

		String enf3 = "Resfriado";
		String gravedad3 = "Comun";

		if (nombre.contains(enf1)) {

			return gravedad1;

		} else if (nombre.equalsIgnoreCase(enf2)) {

			return gravedad2;

		} else if (nombre.equalsIgnoreCase(enf3)) {

			return gravedad3;

		}

		return gravedad;

	}

}
