package clases;

public class Respiratoria extends Enfermedad {

	// Atributos

	private double nvOxigeno;
	private String dificultadRespiratoria;

	// Constructores

	public Respiratoria() {
		super();
		this.nvOxigeno = 0;
		this.dificultadRespiratoria = "";
	}

	public Respiratoria(String origen, String nombre, String gravedad, String caracteristicas, int duracion,
			double nvOxigeno, String dificultadRespiratoria) {

		super(origen, nombre, gravedad, caracteristicas, duracion);

		this.nvOxigeno = nvOxigeno;
		this.dificultadRespiratoria = dificultadRespiratoria;

	}

	// Setters Y Getters

	public double getNvOxigeno() {
		return nvOxigeno;
	}

	public void setNvOxigeno(double nvOxigeno) {
		this.nvOxigeno = nvOxigeno;
	}

	public String getDificultadRespiratoria() {
		return dificultadRespiratoria;
	}

	public void setDificultadRespiratoria(String dificultadRespiratoria) {
		this.dificultadRespiratoria = dificultadRespiratoria;
	}

	public String toString() {
		return "Respiratoria [nvOxigeno=" + nvOxigeno + ", dificultadRespiratoria=" + dificultadRespiratoria + "]";
	}

	// Metodo calcularNvOxigeno

	public double calcularNivelOxigeno(String nombre) {

		String enf1 = "Resfriado comun";
		String enf2 = "Influenza";
		String enf3 = "Faringitis";
		String enf4 = "Bronquitis";
		String enf5 = "Neumon�a";

		double nvOxigeno1 = 98.5;
		double nvOxigeno2 = 89.9;
		double nvOxigeno3 = 97.4;
		double nvOxigeno4 = 96.5;
		double nvOxigeno5 = 91.2;

		if (nombre.equalsIgnoreCase(enf1)) {

			return nvOxigeno1;

		} else if (nombre.equalsIgnoreCase(enf2)) {

			return nvOxigeno2;

		} else if (nombre.equalsIgnoreCase(enf3)) {

			return nvOxigeno3;

		} else if (nombre.equalsIgnoreCase(enf4)) {

			return nvOxigeno4;

		} else if (nombre.equalsIgnoreCase(enf5)) {

			return nvOxigeno5;

		}

		return nvOxigeno;

	}

}
