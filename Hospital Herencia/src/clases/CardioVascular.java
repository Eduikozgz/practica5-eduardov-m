package clases;

public class CardioVascular extends Enfermedad {
	
	private String nvFatiga;
	private int nvfrecuenciaCardiaca;
	
	public  CardioVascular(){
		super();
		this.nvFatiga = "" ;
		this.nvfrecuenciaCardiaca = 0;
	}
	
	public CardioVascular(String origen , String nombre , String gravedad , String caracteristicas , int duracion , String nvFatiga , int nvfrecuenciaCardiaca) {
		
		super( origen ,  nombre ,  gravedad ,  caracteristicas ,  duracion);
		
		this.nvFatiga = nvFatiga ;
		this.nvfrecuenciaCardiaca = nvfrecuenciaCardiaca;
		
	}
	
	
	public String getNvFatiga() {
		return nvFatiga;
	}
	public void setNvFatiga(String nvFatiga) {
		this.nvFatiga = nvFatiga;
	}
	public int getNvfrecuenciaCardiaca() {
		return nvfrecuenciaCardiaca;
	}
	public void setNvfrecuenciaCardiaca(int nvfrecuenciaCardiaca) {
		this.nvfrecuenciaCardiaca = nvfrecuenciaCardiaca;
	}
	
	public String toString() {
		return "CardioVascular [nvFatiga=" + nvFatiga + ", nvfrecuenciaCardiaca=" + nvfrecuenciaCardiaca + "]";
	}
	
	//Metodo calcularNiveldeFatiga
	
	public String calcularNiveldeFatiga(String nombre) {
		
		String enf2 = "Taquicardia";
		String enf3 = "Bradicardia";
		
		String nvFatiga1 = "Elevado";
		String nvFatiga3 = "Bajo";
		
		
		if (nombre.equalsIgnoreCase(enf2)) {

			return nvFatiga1;

		} else if (nombre.equalsIgnoreCase(enf3)) {
			
			return nvFatiga3;
		}

		return nvFatiga;
		
	}

}
